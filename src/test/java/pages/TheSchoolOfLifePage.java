package pages;

import basePage.Pages;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import webKeyword.WebKeyWords;

public class TheSchoolOfLifePage extends Pages {
    public TheSchoolOfLifePage(WebKeyWords action){
        super(action);
        this.action = action;
    }

    @FindBy(xpath = "//nav//*[text()='FOR INDIVIDUALS ']")
    private WebElement INVIDUALS;
    @FindBy(xpath = "//nav//*[text()='1. Psychotherapy']")
    private WebElement PSYCHOTHERAPY;
    @FindBy(xpath = "//button[text()='Allow all cookies']")
    private WebElement ACCEPT;
    @FindBy(xpath = "//*[@class='account-item']//*[text()='Login/Signup']/ancestor::a")
    private WebElement SIGN_IN;
    @FindBy(xpath = "//label[@for='email']/following-sibling::div//input")
    private WebElement EMAIL;




    public TheSchoolOfLifePage clickOnIndividual(){
        action.clickToElement(INVIDUALS);
        return this;
    }

    public TheSchoolOfLifePage clickOnPsychotherapy(){
        action.clickToElement(PSYCHOTHERAPY);
        return this;
    }
    public TheSchoolOfLifePage acceptCookie(){
        action.waitForElementClickable(ACCEPT);
        action.clickToElement(ACCEPT);
        return this;
    }

    public TheSchoolOfLifePage clickSignIn(){
        action.waitForElementClickable(SIGN_IN);
        action.clickToElement(SIGN_IN);
        return this;
    }
    public TheSchoolOfLifePage inputEmail(){
        action.waitForElementVisible(EMAIL);
        action.sendKeyToElement(EMAIL, "12444566");
        EMAIL.sendKeys(Keys.TAB);

        return this;
    }
    public TheSchoolOfLifePage verifyError(String actual){
       String  expected = action.getElementValidationMessage(EMAIL);
        Assert.assertEquals(actual, expected);

        return this;
    }

}
