
package listener;

import driver.DriverManager;
import org.testng.ITestNGListener;
import org.testng.annotations.*;
import pages.*;
import report.AllureManager;

public class AbstractWebTest implements ITestNGListener {

    protected LogInPage logInPage;
    protected SwagLabsPage swagLabsPage;
    protected CartPage cartPage;
    protected InformationUserPage informationUserPage;
    protected CheckOutPage checkOutPage;
    protected CheckOutCompletePage checkOutCompletePage;


    @BeforeMethod
    @Parameters("browser")
    public void setUp(@Optional("chrome") String browser ){
        AllureManager.setAllureEnvironmentInformation(browser,"https://www.saucedemo.com/" );
        DriverManager.createDriver(browser, "https://www.saucedemo.com/");
        logInPage = new LogInPage();
        swagLabsPage = new SwagLabsPage();
        cartPage = new CartPage();
        informationUserPage = new InformationUserPage();
        checkOutPage = new CheckOutPage();
        checkOutCompletePage = new CheckOutCompletePage();

    }
    @AfterMethod
    public void cleanUp(){
        DriverManager.closeBrowserAndDriver(DriverManager.getDriver());
    }

}
