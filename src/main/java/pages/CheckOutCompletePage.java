package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import webKeyword.WebKeyWords;

public class CheckOutCompletePage extends WebKeyWords {
    @FindBy(xpath = "//button[text()='Back Home']")
    public WebElement BACK_HOME_BUTTON;

    @Step("verify check out complete")
    public CheckOutCompletePage verifyCheckOutComplete(){
        String url = getCurrentPageUrl();
        Assert.assertEquals(url, "https://www.saucedemo.com/checkout-complete.html");
        return this;
    }

    @Step("click on back home button")
    public CheckOutCompletePage clickOnBackHomeButton(){
        clickToElement(BACK_HOME_BUTTON);
        return this;
    }
}
