package basePage;


import config.ConfigSettings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import webKeyword.WebKeyWords;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;


public class Pages {
	protected WebKeyWords action;
	protected HashMap<String, String> repoFile;
	protected ConfigSettings config;
	

	protected Pages() {
//		action = new WebKeyWords();
//		PageFactory.initElements(new AjaxElementLocatorFactory(action.getWebDriver(), 30), this);
//		config = new ConfigSettings(System.getProperty("user.dir"));
	}

	protected Pages(WebKeyWords action) {
		this();
		this.action = action;
		config = new ConfigSettings(System.getProperty("user.dir"));
	}

	/**
	 * @param strRepoFile
	 * @return
	 */
	public String setRepoFile(String strRepoFile) {
		if (this.repoFile == null) {
			this.repoFile = new HashMap<>();
		}
		// String strClassName = Reflection.getCallerClass(2).getSimpleName();
		String strClassName = getCallerClass(2).getSimpleName();

//	    Json file
		strRepoFile = strRepoFile + File.separator + strClassName + ".json"; // Repo file relative path: object_repository\W3SchoolSignUp.json
//		logger.info("Repo file relative path: " + strRepoFile);
//		strRepoFile = FileHelper.getCorrectJsonFilePath(strRepoFile);
		return this.repoFile.put(strClassName, strRepoFile);
	}

	private static Class<?> getCallerClass(final int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException(Integer.toString(index));
		}
		try {
			return getCallerClassFromStackTrace(index + 1);
		} catch (final ClassNotFoundException e) {
//			logger.error("Could not find class in ReflectionUtil.getCallerClass({}), index<" + index + ">"
//					+ ", exception< " + e + ">");
		}
		return null;
	}

	private static Class<?> getCallerClassFromStackTrace(final int index) throws ClassNotFoundException {

		final StackTraceElement[] elements = new Throwable().getStackTrace();
		int i = 0;
		for (final StackTraceElement element : elements) {
			if (isValidlMethod(element)) {
				if (i == index) {
					return Class.forName(element.getClassName());
				}
				++i;
			}
		}
		throw new IndexOutOfBoundsException(Integer.toString(index));
	}

	private static boolean isValidlMethod(final StackTraceElement element) {
		if (element.isNativeMethod()) {
			return false;
		}
		final String cn = element.getClassName();
		if (cn.startsWith("sun.reflect.")) {
			return false;
		}
		final String mn = element.getMethodName();
		if (cn.startsWith("java.lang.reflect.") && (mn.equals("invoke") || mn.equals("newInstance"))) {
			return false;
		}
		if (cn.equals("java.lang.Class") && mn.equals("newInstance")) {
			return false;
		}
		return true;
	}
	
	/**
	   * @return
	   */
	  public String getRepoFile() {
	    String strClassName = getCallerClass(3).getSimpleName();
	    return this.repoFile.get(strClassName);
	  }


	public String getProductLink(WebElement we) {
		// TODO Auto-generated method stub
		return null;
	}




}
