package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;

public class ChromeDriverManager {
    private static ThreadLocal<WebDriver> driver = new ThreadLocal();
    public WebDriver getDriver() {
        return driver.get();
    }

    public static void setDriver(WebDriver webDriver) {
        driver.set(webDriver);
    }
    protected void createDriver(String browserName) {
        switch (browserName){
            case "chrome":

        }
        WebDriver tmpDriver = null;
        WebDriverManager.chromedriver().setup();
        String language = "en_GB";
        ChromeOptions option = new ChromeOptions();
        Map<String, Object> chrome_prefs = new HashMap<>();
        chrome_prefs.put("intl.accept_languages", language);
        option.addArguments(new String[]{"--incognito"});
        option.addArguments(new String[]{"--disable-extensions"});
        option.addArguments(new String[]{"--disable-infobars"});
        option.setExperimentalOption("prefs", chrome_prefs);
        tmpDriver = new ChromeDriver();
        setDriver((WebDriver)tmpDriver);
    }
}
