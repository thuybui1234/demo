package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class DriverManager {
    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    private static int LONG_TIMEOUT = 30;

    public static WebDriver getDriver() {
        return driver.get();
    }
    public static void setDriver(WebDriver webDriver) {
        driver.set(webDriver);
    }
    public static void createDriver(String browserName, String url) {
        WebDriver tmpDriver = null;
        switch (browserName){
            case "chrome":
                WebDriverManager.chromedriver().setup();
                String language = "en_GB";
                ChromeOptions option = new ChromeOptions();
                Map<String, Object> chrome_prefs = new HashMap<>();
                chrome_prefs.put("intl.accept_languages", language);
                option.addArguments("--incognito");
                option.addArguments("--disable-extensions");
                option.addArguments("--disable-infobars");
                option.setExperimentalOption("prefs", chrome_prefs);
                tmpDriver = new ChromeDriver();
                break;
        }
        assert tmpDriver != null;
        tmpDriver.get(url);
        setDriver(tmpDriver);
        getDriver().manage().timeouts().implicitlyWait(getLONG_TIMEOUT(), TimeUnit.SECONDS);
        getDriver().manage().window().maximize();

    }

    private static int getLONG_TIMEOUT() {
        return LONG_TIMEOUT;
    }
    public static void closeBrowserAndDriver(WebDriver driver){
        driver.manage().deleteAllCookies();
        driver.quit();

    }





}
